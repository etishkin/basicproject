﻿using System;
using System.Security.Claims;
using System.Security.Principal;
using Microsoft.AspNet.Identity;

namespace BasicProject.Site.Security
{
    public static class IdentityExtension 
    {
        public static int GetUserId(this IIdentity identity)
        {
            if (identity == null)
            {
                throw new ArgumentNullException("identity");
            }
            var ci = identity as ClaimsIdentity;
            if (ci != null)
            {
                return Convert.ToInt32(ci.FindFirstValue(ClaimTypes.NameIdentifier));
            }
            return 0;
        }
    }
}