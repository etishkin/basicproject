﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BasicProject.Site.Startup))]
namespace BasicProject.Site
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
