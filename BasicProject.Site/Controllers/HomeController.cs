﻿using System.Web.Mvc;
using BasicProject.Core.DAL;

namespace BasicProject.Site.Controllers
{
    public class HomeController : BasicController
    {
        public HomeController(IServicesFactory dal) : base(dal)
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}