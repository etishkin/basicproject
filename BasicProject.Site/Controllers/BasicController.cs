﻿using System;
using System.Web.Mvc;
using BasicProject.Core.DAL;
using Microsoft.AspNet.Identity;

namespace BasicProject.Site.Controllers
{
    public class BasicController : Controller
    {
        protected IServicesFactory DAL;

        public BasicController(IServicesFactory dal)
        {
            DAL = dal;
        }

        public T CurrentUserId<T>() where T : IConvertible
        {
            return User.Identity.GetUserId<T>();
        }

        public int UserId
        {
            get { return CurrentUserId<int>(); }
        }

        public ApplicationUser CurrentUser
        {
            get { return DAL.UserService.Value.Find(UserId); }
        }
}
}