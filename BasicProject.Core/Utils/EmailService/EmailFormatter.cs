﻿using System;
using System.Web;

namespace BasicProject.Core.Utils
{
    public static class EmailFormatter
    {
        private static readonly string HostName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);

        public static string ResetPassword(string password, string token)
        {
            var link = string.Format("{0}/Account/ResetPassword/{1}", HostName, token);
            return string.Format("<p>Ваш новый пароль для входа в систему: <b>{1}</b></p>" +
                                 "<p>Для подтверждения смены пароля перейдите по ссылке: <a href=\"{0}\">{0}</a></p>" +
                                 "<p>Если Вы не запрашивали изменение пароля, то просто проигнорируйте данное сообщение.</p>" +
                                 "<p>Ссылка действительна в течении 24 часов</p>",
                link, password);
        }

        public static string Register(string email, string password)
        {
            return string.Format("<p>Email: <b>{1}</b></p>" +
                                 "<p>Ваш пароль для входа в систему: <b>{0}</b></p>" +
                                 "<p>Никому не сообщайте свой пароль! Изменить пароль Вы можете в личном кабинете.</p>",
                password, email);
        }
    }
}