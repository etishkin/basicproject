﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace BasicProject.Core.Utils.EmailService
{
    public class Notify
    {
        public static void With<T>(params object[] args)
        {
            var service = (IMessageService)Activator.CreateInstance(typeof(T), args);
            if (service == null) throw new ArgumentException();
            service.Send();
        }
    }

    public interface IMessageService
    {
        IMessageConfigurator Configurator { get; }
        void Configure();
        void Send();
    }

    public interface IMessageConfigurator
    {
        string PrepareHeader(string header);
        string PrepareBody(string body);
    }

    public interface IMessageConfiguratorRegisterFor
    {
        IMessageConfiguratorRegisterUse For<T>() where T : class, IMessageService;
    }

    public interface IMessageConfiguratorRegisterUse
    {
        void Use<T>() where T : class, IMessageConfigurator;
    }

    public class MessageConfiguratorRegister : IMessageConfiguratorRegisterFor, IMessageConfiguratorRegisterUse
    {
        protected RegistredMessageConfigurator Registred = null;

        public bool IsConfigured
        {
            get { return Registred != null && Registred.ConfiguratorType != null && Registred.ServiceType != null; }
        }

        public IMessageConfiguratorRegisterUse For<T>() where T : class, IMessageService
        {
            Registred = new RegistredMessageConfigurator { ServiceType = typeof(T) };
            return this;
        }

        public void Use<T>() where T : class, IMessageConfigurator
        {
            Registred.ConfiguratorType = typeof(T);

            if (IsConfigured) MessageConfiguratorFactory.Register(Registred);
        }
    }

    public static class MessageConfiguratorFactory
    {
        static MessageConfiguratorFactory()
        {
            RegistredMessageConfigurators = new List<RegistredMessageConfigurator>();
        }

        private static List<RegistredMessageConfigurator> RegistredMessageConfigurators;
        public static void Register(Action<IMessageConfiguratorRegisterFor> action)
        {
            var registerModel = new MessageConfiguratorRegister();
            action.Invoke(registerModel);
        }

        internal static void Register<T1, T2>()
            where T1 : class, IMessageConfigurator
            where T2 : class, IMessageService
        {
            RegistredMessageConfigurators.Add(new RegistredMessageConfigurator
            {
                ConfiguratorType = typeof(T1),
                ServiceType = typeof(T2)
            });
        }

        internal static void Register(RegistredMessageConfigurator registred)
        {
            RegistredMessageConfigurators.Add(registred);
        }

        public static IMessageConfigurator GetFor<T>() where T : IMessageService
        {
            return GetFor(typeof(T));
        }

        public static IMessageConfigurator GetFor(Type T)
        {
            if (!typeof(IMessageService).IsAssignableFrom(T)) throw new ArgumentException();
            var registredModel = RegistredMessageConfigurators.FirstOrDefault(r => r.ServiceType == T);
            if (registredModel == null)
            {
                return null;
            }
            var type = registredModel.ConfiguratorType;
            return (IMessageConfigurator)Activator.CreateInstance(type);
        }
    }

    public class RegistredMessageConfigurator
    {
        public Type ServiceType { get; set; }
        public Type ConfiguratorType { get; set; }
    }

    public abstract class BasicMessageService : IMessageService
    {
        public IEnumerable<string> Targets { get; protected set; }
        public string Header { get; set; }
        public string Body { get; set; }
        public IMessageConfigurator Configurator { get; protected set; }

        #region Constructors

        protected BasicMessageService(IEnumerable<string> targets, string header, string body, IMessageConfigurator configurator)
        {
            Targets = targets;
            Header = header;
            Body = body;
            Configurator = configurator;
            // ReSharper disable once DoNotCallOverridableMethodsInConstructor
            Configure();
        }

        protected BasicMessageService(string target, string header, string body, IMessageConfigurator configurator)
            : this(new List<string> { target }, header, body, configurator)
        { }

        protected BasicMessageService(IEnumerable<string> targets, string header, string body)
            : this(targets, header, body, null)
        {
            Configurator = MessageConfiguratorFactory.GetFor(GetType());
            // ReSharper disable once DoNotCallOverridableMethodsInConstructor
            Configure();
        }
        protected BasicMessageService(string target, string header, string body) : this(new List<string> { target }, header, body) { }
        protected BasicMessageService(IEnumerable<string> targets, string body) : this(targets, string.Empty, body) { }
        protected BasicMessageService(string target, string body) : this(target, string.Empty, body) { }
        protected BasicMessageService(IEnumerable<string> targets, string body, IMessageConfigurator configurator) : this(targets, string.Empty, body, configurator) { }
        protected BasicMessageService(string target, string body, IMessageConfigurator configurator) : this(target, string.Empty, body, configurator) { }

        #endregion

        public virtual void Configure()
        {
            if (Configurator == null) return;
            Header = Configurator.PrepareHeader(Header);
            Body = Configurator.PrepareBody(Body);
        }

        public abstract void Send();
    }

    public class EmailConfigurator : IMessageConfigurator
    {
        public string PrepareHeader(string header)
        {
            return string.Format("{0} [Nestro credit notes]", header);
        }

        public string PrepareBody(string body)
        {
            return string.Format("{0}", body);
        }
    }
    public class EmailService : BasicMessageService
    {
        private const string DefaultSmtpPort = "25";

        private static void Send(string smtpServer, uint port, bool useSSL, string from, string password, string mailto, string replyTo, string caption, string message, string attachFile = null)
        {
            try
            {
                var mail = CreateMessage(@from, mailto, caption, message, replyTo);

                if (!string.IsNullOrEmpty(attachFile))
                    mail.Attachments.Add(new Attachment(attachFile));

                SmtpSend(smtpServer, port, useSSL, @from, password, mail);

                mail.Dispose();
            }
            catch (SmtpException e)
            {
                Debug.WriteLine(string.Format("{0}: {1}", "SmtpException", e.Message));
            }
        }

        private static void SmtpSend(string smtpServer, uint port, bool useSSL, string @from, string password, MailMessage mail)
        {
            var client = new SmtpClient
            {
                Host = smtpServer,
                Port = (int)port,
                EnableSsl = useSSL,
                Credentials = new NetworkCredential(@from, password),
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
            client.Send(mail);

            Debug.WriteLine(string.Format("{0} Email Sent To: {1}", mail.Subject, mail.To));
        }

        private static MailMessage CreateMessage(string @from, string mailto, string caption, string message, string replyTo = "")
        {
            var mail = new MailMessage
            {
                From = new MailAddress(@from),
                Subject = caption,
                Body = message,
                IsBodyHtml = true
            };

            if (!string.IsNullOrWhiteSpace(replyTo))
                mail.ReplyToList.Add(new MailAddress(replyTo));

            mail.To.Add(new MailAddress(mailto));
            return mail;
        }

        private static bool Send(string mailto, string caption, string message)
        {
            var from = ConfigHelper.GetAppSetting("SmtpLogin");
            var replyTo = ConfigHelper.GetAppSetting("ReplyTo");
            var smtpServer = ConfigHelper.GetAppSetting("SmtpHost");
            var password = ConfigHelper.GetAppSetting("SmtpPassword");
            var smtpUseSsl = ConfigHelper.CheckBoolAppSetting("SmtpSSL");

            uint smtpPort;
            if (!UInt32.TryParse(ConfigHelper.GetAppSetting("SmtpPort", DefaultSmtpPort), NumberStyles.Integer, CultureInfo.InvariantCulture, out smtpPort))
                return false;

            Send(smtpServer, smtpPort, smtpUseSsl, from, password, mailto, replyTo, caption, message);
            Debug.WriteLine("{0} : {1} - {2}", mailto, caption, message);
            return true;
        }


        #region Constructors

        public EmailService(IEnumerable<string> targets, string header, string body, IMessageConfigurator configurator)
            : base(targets, header, body, configurator)
        {
        }

        public EmailService(string target, string header, string body, IMessageConfigurator configurator)
            : base(target, header, body, configurator)
        {
        }

        public EmailService(IEnumerable<string> targets, string header, string body)
            : base(targets, header, body)
        {
        }

        public EmailService(string target, string header, string body)
            : base(target, header, body)
        {
        }

        public EmailService(IEnumerable<string> targets, string body)
            : base(targets, body)
        {
        }

        public EmailService(string target, string body)
            : base(target, body)
        {
        }

        public EmailService(IEnumerable<string> targets, string body, IMessageConfigurator configurator)
            : base(targets, body, configurator)
        {
        }

        public EmailService(string target, string body, IMessageConfigurator configurator)
            : base(target, body, configurator)
        {
        }

        #endregion

        public override void Send()
        {
            foreach (var target in Targets)
            {
                Send(target, Header, Body);
            }
        }
    }
}