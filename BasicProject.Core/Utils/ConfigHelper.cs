﻿using System.Linq;
using System.Web.Configuration;

namespace BasicProject.Core.Utils
{
    public static class ConfigHelper
    {
        /// <summary>
        /// Метод для проверки параметра-флага в Web.Config на истинность
        /// </summary>
        /// <param name="settingName">название параметра</param>
        /// <param name="checkValue">значение для проверки истинности. По умолчанию "true"</param>
        /// <returns></returns>
        public static bool CheckBoolAppSetting(string settingName, string checkValue = "true")
        {
            return IsAppSettingExist(settingName) &&
                   WebConfigurationManager.AppSettings[settingName].Equals(checkValue);
        }

        /// <summary>
        /// Метод для получения значения параметра из Web.Config
        /// </summary>
        /// <param name="settingName">название параметра</param>
        /// <param name="defaultValue">значение по умолчанию</param>
        /// <returns></returns>
        public static string GetAppSetting(string settingName, string defaultValue = "")
        {
            return IsAppSettingExist(settingName) ? WebConfigurationManager.AppSettings[settingName] : defaultValue;
        }

        /// <summary>
        /// Метод для получения числового значения параметра из Web.Config
        /// </summary>
        /// <param name="settingName">название параметра</param>
        /// <param name="defaultValue">значение по умолчанию</param>
        /// <returns></returns>
        public static int CheckIntAppSetting(string settingName, int defaultValue = 0)
        {
            var value = defaultValue;
            int.TryParse(GetAppSetting(settingName, defaultValue.ToString()), out value);
            return value;
        }

        /// <summary>
        /// Проверка значения параметра на существование и не-пустоту
        /// </summary>
        /// <param name="settingName">название параметра</param>
        /// <returns></returns>
        public static bool IsAppSettingExist(string settingName)
        {
            return WebConfigurationManager.AppSettings.HasKeys() &&
                   WebConfigurationManager.AppSettings.AllKeys.Any((k) => k.Equals(settingName)) &&
                   !string.IsNullOrWhiteSpace(WebConfigurationManager.AppSettings[settingName]);
        }
    }
}