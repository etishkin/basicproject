﻿using System;
using System.Linq;

namespace BasicProject.Core.Utils
{
    public static class StringExtension
    {
        public static string ShuffleString(this string stringToShuffle)
        {
            if (String.IsNullOrEmpty(stringToShuffle))
            {
                throw new ArgumentNullException("stringToShuffle",
                                                "The stringToShuffle variable must not be null or empty");
            }

            return new string(stringToShuffle.OrderBy(character => Guid.NewGuid()).ToArray());
        }

        public static byte[] FromHexstringToByteArray(this string hex)
        {
            var length = hex.Length;
            if (length % 2 == 1)
                throw new Exception("The binary key cannot have an odd number of digits");

            var arr = new byte[hex.Length >> 1];

            for (var i = 0; i < (length >> 1); ++i)
            {
                arr[i] = (byte)((GetHexVal(hex[i << 1]) << 4) + (GetHexVal(hex[(i << 1) + 1])));
            }

            return arr;
        }

        public static int GetHexVal(char hex)
        {
            int val = (int)hex;
            //For uppercase A-F letters:
            return val - (val < 58 ? 48 : 55);
            //For lowercase a-f letters:
            //return val - (val < 58 ? 48 : 87);
            //Or the two combined, but a bit slower:
            //return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
        }

        public static bool IsEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }
    }
}