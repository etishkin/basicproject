﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BasicProject.Core.Utils
{
    public class MnemonicPasswordGenerator
    {
        private string _password;
        public string Password
        {
            get { return _password; }
            private set
            {
                _password = value;
                GenerateMnemonicText();
            }
        }
        public string MnemonicText { get; private set; }

        private const string Alphabet = "abcdefghigklmnopqrstuvwxyz";
        private const string Numbers = "0123456789";
        private const string Symbols = @"!@#$%^&*()-=+_.,/\";
        private readonly Hashtable _mnemonicDescription = new Hashtable();
        private readonly Random _randomizer;
        public MnemonicPasswordGenerator()
        {
            _mnemonicDescription.Add('A', "Alfa");
            _mnemonicDescription.Add('B', "Bravo");
            _mnemonicDescription.Add('C', "Charlie");
            _mnemonicDescription.Add('D', "Delta");
            _mnemonicDescription.Add('E', "Echo");
            _mnemonicDescription.Add('F', "Foxtrot");
            _mnemonicDescription.Add('G', "Golf");
            _mnemonicDescription.Add('H', "Hotel");
            _mnemonicDescription.Add('I', "India");
            _mnemonicDescription.Add('J', "Juliett");
            _mnemonicDescription.Add('K', "Kilo");
            _mnemonicDescription.Add('L', "Lima");
            _mnemonicDescription.Add('M', "Mike");
            _mnemonicDescription.Add('N', "November");
            _mnemonicDescription.Add('O', "Oscar");
            _mnemonicDescription.Add('P', "Papa");
            _mnemonicDescription.Add('Q', "Quebec");
            _mnemonicDescription.Add('R', "Romeo");
            _mnemonicDescription.Add('S', "Sierra");
            _mnemonicDescription.Add('T', "Tango");
            _mnemonicDescription.Add('U', "Uniform");
            _mnemonicDescription.Add('V', "Victor");
            _mnemonicDescription.Add('W', "Whiskey");
            _mnemonicDescription.Add('X', "X-ray");
            _mnemonicDescription.Add('Y', "Yankee");
            _mnemonicDescription.Add('Z', "Zulu");
            _mnemonicDescription.Add('1', "One");
            _mnemonicDescription.Add('2', "Two");
            _mnemonicDescription.Add('3', "Three");
            _mnemonicDescription.Add('4', "Four");
            _mnemonicDescription.Add('5', "Five");
            _mnemonicDescription.Add('6', "Six");
            _mnemonicDescription.Add('7', "Seven");
            _mnemonicDescription.Add('8', "Eight");
            _mnemonicDescription.Add('9', "Nine");
            _mnemonicDescription.Add('0', "Zero");

            _randomizer = new Random();
        }

        public void Generate(int length = 8, bool useDifferentCases = true, bool useNumbers = true, bool useSpecialSymbols = false)
        {
            var alphabets = FillAlphabets(useDifferentCases, useNumbers, useSpecialSymbols);
            var pwd = string.Empty;
            var averageCount = length / alphabets.Count;
            var unusedSymbols = length;
            if (averageCount != 0)
            {
                for (var i = 0; i < alphabets.Count; ++i)
                {
                    pwd += GenerateStringWithAlphabet(averageCount, alphabets[i]);
                }
                unusedSymbols -= averageCount * alphabets.Count;
            }
            pwd += GenerateStringWithAlphabets(unusedSymbols, alphabets);
            Password = pwd.ShuffleString();
        }

        private List<string> FillAlphabets(bool useDifferentCases, bool useNumbers, bool useSpecialSymbols)
        {
            var usedAlphabets = new List<string> { Alphabet };
            AddAlphabet(ref usedAlphabets, useDifferentCases, Alphabet.ToUpper());
            AddAlphabet(ref usedAlphabets, useNumbers, Numbers);
            AddAlphabet(ref usedAlphabets, useSpecialSymbols, Symbols);
            return usedAlphabets;
        }

        private void AddAlphabet(ref List<string> list, bool condition, string alphabet)
        {
            if (condition) list.Add(alphabet);
        }

        private string GenerateStringWithAlphabet(int length, string alphabet)
        {
            var str = string.Empty;
            for (var i = 0; i < length; ++i)
            {
                str += GetRandomSymbol(alphabet);
            }
            return str;
        }

        private char GetRandomSymbol(string alphabet)
        {
            return alphabet[_randomizer.Next(alphabet.Length)];
        }

        private string GenerateStringWithAlphabets(int length, List<string> alphabets)
        {
            var pwd = string.Empty;
            for (var i = 0; i < length; ++i)
            {
                pwd += GetRandomSymbol(alphabets[_randomizer.Next(alphabets.Count)]);
            }
            return pwd;
        }

        private void GenerateMnemonicText()
        {
            if (Password.IndexOfAny(Symbols.ToCharArray()) != -1)
            {
                MnemonicText = "Password contain special symbols";
                return;
            }

            var helper = string.Empty;
            const string patternWithSpace = " {0}";
            const string patternWithoutSpace = "{0}";
            foreach (var currentChar in Password)
            {
                if (Char.IsDigit(currentChar) || Char.IsUpper(currentChar))
                    helper += string.Format(helper.IsEmpty() ? patternWithoutSpace : patternWithSpace, _mnemonicDescription[Char.ToUpper(currentChar)]);
                else
                    helper += string.Format(helper.IsEmpty() ? patternWithoutSpace : patternWithSpace, _mnemonicDescription[Char.ToUpper(currentChar)]).ToLower();
            }
            MnemonicText = helper;
        }
    }
}
