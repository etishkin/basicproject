﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using BasicProject.Core.DAL.Conventions;
using BasicProject.Core.Logger;
using Microsoft.AspNet.Identity.EntityFramework;

namespace BasicProject.Core.DAL
{
    public interface IApplicationDbContext
    {
        void SetModified(object entity);
        void SetDeleted(object entity);
        Database Database { get; }
        DbChangeTracker ChangeTracker { get; }
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbSet Set(Type entityType);
        int SaveChanges();
        IEnumerable<DbEntityValidationResult> GetValidationErrors();
        DbEntityEntry Entry(object entity);
        void Dispose();
    }

    public partial class ApplicationDbContext : IdentityDbContext<ApplicationUser, CustomRole, int, CustomUserLogin, CustomUserRole, CustomUserClaim>, IApplicationDbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
            Configuration.ProxyCreationEnabled = true;
            Configuration.LazyLoadingEnabled = true;
            _logger = new Logger.Logger(this);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Add<DateTimeConvention>();
            modelBuilder.Conventions.Add<IdConvention>();
            modelBuilder.Conventions.Add<DecimalConvention>();

            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
        }

        private readonly ILogger _logger;

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }

    public partial class ApplicationDbContext : IDbContext
    {
        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                Debug.WriteLine(sb);
                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb.ToString(), ex
                    ); // Add the original exception as the innerException
            }
        }
        public virtual void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public virtual void SetDeleted(object entity)
        {
            Entry(entity).State = EntityState.Deleted;
        }

        public void MarkAsAdded<T>(T entity) where T : class
        {
            Entry(entity).State = EntityState.Added;
            Set<T>().Add(entity);
        }

        public void MarkAsDeleted<T>(T entity) where T : class
        {
            var iDbEntity = entity as IDbEntity;
            if (iDbEntity == null)
                throw new ArgumentException("entity should be IDbEntity type", "entity");

            iDbEntity.State = DbEntityState.Inactive;
            MarkAsModified((T)iDbEntity);
        }

        public void MarkAsModified<T>(T entity) where T : class
        {
            Attach(entity);
            Entry(entity).State = EntityState.Modified;
        }

        public void Attach<T>(T entity) where T : class
        {
            if (Entry(entity).State == EntityState.Detached)
            {
                Set<T>().Attach(entity);
            }
        }

        public void Commit(bool withLogging)
        {
            BeforeCommit();
            if (withLogging)
            {
                _logger.Run();
            }
            SaveChanges();
        }

        private void BeforeCommit()
        {
            UndoExistAddedEntitys();
        }

        private void UndoExistAddedEntitys()
        {
            IEnumerable<DbEntityEntry> dbEntityEntries = GetChangeTracker().Entries().Where(x => x.State == EntityState.Added);
            foreach (var dbEntityEntry in dbEntityEntries)
            {
                if (GetKeyValue(dbEntityEntry.Entity) > 0)
                {
                    SetEntityState(dbEntityEntry.Entity, EntityState.Unchanged);
                }
            }
        }

        public void Rollback()
        {
            ChangeTracker.Entries().ToList().ForEach(x => x.Reload());
        }

        public void EnableTracking(bool isEnable)
        {
            Configuration.AutoDetectChangesEnabled = isEnable;
        }

        public void SetEntityState<T>(T entity, EntityState state) where T : class
        {
            Entry(entity).State = state;
        }

        public DbChangeTracker GetChangeTracker()
        {
            return ChangeTracker;
        }

        public EntityState GetEntityState<T>(T entity) where T : class
        {
            return Entry(entity).State;
        }

        public IQueryable<T> Find<T>() where T : class
        {
            return Set<T>();
        }

        public DbEntityEntry GetDbEntry<T>(T entity) where T : class
        {
            return Entry(entity);
        }

        public static int GetKeyValue<T>(T entity) where T : class
        {
            var dbEntity = entity as IDbEntity;
            if (dbEntity == null)
                throw new ArgumentException("Entity should be IDbEntity type - " + entity.GetType().Name);

            return dbEntity.Id;
        }
    }

    public interface IDbContext
    {
        IQueryable<T> Find<T>() where T : class;
        void MarkAsAdded<T>(T entity) where T : class;
        void MarkAsDeleted<T>(T entity) where T : class;
        void MarkAsModified<T>(T entity) where T : class;
        void Commit(bool withLogging);
        void Rollback();
        void EnableTracking(bool isEnable);
        EntityState GetEntityState<T>(T entity) where T : class;
        void SetEntityState<T>(T entity, EntityState state) where T : class;
        DbChangeTracker GetChangeTracker();
        DbEntityEntry GetDbEntry<T>(T entity) where T : class;
    }

    public interface IDbEntity
    {
        int Id { get; set; }
        DbEntityState State { get; set; }
    }

    public enum DbEntityState
    {
        Active,
        Inactive,
    }

    public class CustomUserRole : IdentityUserRole<int> { }
    public class CustomUserClaim : IdentityUserClaim<int> { }
    public class CustomUserLogin : IdentityUserLogin<int> { }

    public class CustomRole : IdentityRole<int, CustomUserRole>
    {
        public CustomRole() { }
        public CustomRole(string name) { Name = name; }
    }

    public class CustomUserStore : UserStore<ApplicationUser, CustomRole, int,
        CustomUserLogin, CustomUserRole, CustomUserClaim>
    {
        public CustomUserStore(ApplicationDbContext context)
            : base(context)
        {
        }
    }

    public class CustomRoleStore : RoleStore<CustomRole, int, CustomUserRole>
    {
        public CustomRoleStore(ApplicationDbContext context)
            : base(context)
        {
        }
    }
}