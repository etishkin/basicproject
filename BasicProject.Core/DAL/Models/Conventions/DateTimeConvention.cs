﻿using System;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace BasicProject.Core.DAL.Conventions
{
    public class DateTimeConvention : Convention
    {
        public DateTimeConvention()
        {
            Properties<DateTime>()
                .Configure(c => c.HasColumnType("datetime2"));
        }
    }
}