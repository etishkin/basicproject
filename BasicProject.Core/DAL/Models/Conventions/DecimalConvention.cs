﻿using System.Data.Entity.ModelConfiguration.Conventions;

namespace BasicProject.Core.DAL.Conventions
{
    public class DecimalConvention : Convention
    {
        public DecimalConvention()
        {
            Properties<decimal>().Configure(c => c.HasPrecision(15, 2));
        }
    }
}