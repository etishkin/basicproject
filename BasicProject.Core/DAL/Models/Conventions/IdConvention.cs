﻿using System;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace BasicProject.Core.DAL.Conventions
{
    public class IdConvention : Convention
    {
        public IdConvention()
        {
            Properties<int>().Where(p => p.Name.Equals("Id", StringComparison.OrdinalIgnoreCase)).Configure(c => c.IsKey());
        }
    }
}