﻿using System;

namespace BasicProject.Core.DAL
{
    public interface IServicesFactory
    {
        Lazy<IUserService> UserService { get; set; }     
    }

    public class ServicesFactory: IServicesFactory
    {
        public ServicesFactory(Lazy<IUserService> userService)
        {
            UserService = userService;
        }

        public Lazy<IUserService> UserService { get; set; }
    }
}
