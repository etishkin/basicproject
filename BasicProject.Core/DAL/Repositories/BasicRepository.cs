﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;

namespace BasicProject.Core.DAL
{
    public interface IBasicRepository<T> where T : class, IDbEntity
    {
        DbEntityValidationResult Validate(T entity);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(DbEntityState state);
        T Find(int entityId);
        T Find(int entityId, DbEntityState state);
        T SaveOrUpdate(T entity);
        T Add(T entity);
        T Update(T entity);
        void Delete(T entity);
        void Delete(int entityId);
        void Restore(T entity);
        void Restore(int entityId);
        bool IsActive(int entityId);
    }

    public abstract class ContextManger
    {
        protected readonly ApplicationDbContext Context;

        protected ContextManger(IApplicationDbContext connection)
        {
            Context = (ApplicationDbContext)connection;
        }
    }

    public abstract class BasicRepository<T> : ContextManger, IBasicRepository<T> where T : class, IDbEntity
    {
        protected BasicRepository(IApplicationDbContext connection)
            : base(connection)
        {
        }

        public DbEntityValidationResult Validate(T entity)
        {
            return Context.Entry(entity).GetValidationResult();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return Context.Set<T>().ToList();
        }

        public virtual IEnumerable<T> GetAll(DbEntityState state)
        {
            return Context.Set<T>().Where(t => t.State == state).ToList();
        }

        public virtual T Find(int entityId, DbEntityState state)
        {
            return Context.Set<T>().FirstOrDefault(t => t.Id == entityId && t.State == state);
        }

        public virtual T Find(int entityId)
        {
            return Context.Set<T>().Find(entityId);
        }

        public virtual T SaveOrUpdate(T entity)
        {
            var iDbEntity = entity as IDbEntity;

            if (iDbEntity == null)
                throw new ArgumentException("entity should be IDbEntity type", "entity");

            return iDbEntity.Id == 0 ? Add(entity) : Update(entity);
        }

        public virtual T Add(T entity)
        {
            var ctx = Context as IDbContext;
            if (ctx == null)
                throw new ArgumentException("Context should be IDbContext type", "Context");

            //BeforeSave(entity, Context);
            ctx.MarkAsAdded(entity);
            ctx.Commit(true);
            return entity;
        }

        public virtual T Update(T entity)
        {
            var iDbEntity = entity as IDbEntity;
            if (iDbEntity == null)
                throw new ArgumentException("entity should be IDbEntity type", "entity");

            var attachedEntity = Context.Set<T>().Find(iDbEntity.Id);
            Context.Entry(attachedEntity).CurrentValues.SetValues(entity);

            //BeforeSave(attachedEntity, Context);
            (Context as IDbContext).Commit(true);
            return entity;
        }

        public virtual void Delete(T entity)
        {
            var ctx = Context as IDbContext;
            if (ctx == null)
                throw new ArgumentException("Context should be IDbContext type", "Context");

            ctx.MarkAsDeleted(entity);
            ctx.Commit(true);
        }

        public virtual void Delete(int entityId)
        {
            var entity = Find(entityId);
            Delete(entity);
        }

        public void Restore(T entity)
        {
            var ctx = Context as IDbContext;
            if (ctx == null)
                throw new ArgumentException("Context should be IDbContext type", "Context");

            var iDbEntity = entity as IDbEntity;
            if (iDbEntity == null)
                throw new ArgumentException("entity should be IDbEntity type", "entity");

            entity.State = DbEntityState.Active;
            ctx.Commit(true);
        }

        public virtual void Restore(int entityId)
        {
            var entity = Find(entityId);
            Restore(entity);
        }

        public virtual bool IsActive(int entityId)
        {
            return Find(entityId, DbEntityState.Active) != null;
        }
    }
}