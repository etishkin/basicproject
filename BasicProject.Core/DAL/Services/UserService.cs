﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BasicProject.Core.Utils;
using BasicProject.Core.Utils.EmailService;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace BasicProject.Core.DAL
{
    public interface IUserService : IBasicRepository<ApplicationUser>
    {
        ApplicationUser Find(string email);
        void Register(ApplicationUser user, string password);
        void Register(ApplicationUser user);
        void ChangePassword(int userId);
        void ChangePassword(string email);
        Task<SignInStatus> PasswordLogin(string email, string password);
    }

    public class UserService : BasicRepository<ApplicationUser>, IUserService
    {
        protected readonly ApplicationUserManager UserManager;
        protected readonly MnemonicPasswordGenerator Generator;

        public UserService(IApplicationDbContext context)
            : base(context)
        {
            Debug.WriteLine("UserService");
            Generator = new MnemonicPasswordGenerator();
        }

        public void Register(ApplicationUser user, string password)
        {

        }

        public void Register(ApplicationUser user)
        {
            Generator.Generate();
            Register(user, Generator.Password);
        }

        public override IEnumerable<ApplicationUser> GetAll()
        {
            return Context.Users.ToList();
        }

        public override IEnumerable<ApplicationUser> GetAll(DbEntityState state)
        {
            return Context.Users.Where(u => u.State == state).ToList();
        }

        public void ChangePassword(string email)
        {
            var user = Find(email);
            ChangePassword(user);
        }

        public void ChangePassword(int userId)
        {
            var user = Find(userId);
            ChangePassword(user);
        }

        private void ChangePassword(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public ApplicationUser Find(string email)
        {
            return Context.Users.FirstOrDefault(u => u.Email.Equals(email, StringComparison.OrdinalIgnoreCase));
        }

        public async Task<SignInStatus> PasswordLogin(string email, string password)
        {
            var user = Find(email);
            if (user == null || user.State == DbEntityState.Inactive) return SignInStatus.Failure;
            if (await UserManager.CheckPasswordAsync(user, password))
            {
                UserManager.ResetAccessFailedCount(user.Id);
                return SignInStatus.Success;
            }
            else
            {
                var result = await UserManager.AccessFailedAsync(user.Id);
                return SignInStatus.Failure;
            }
        }
    }
    
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            Debug.Write("Email send");
            return Task.Run(() => Notify.With<Utils.EmailService.EmailService>(message.Destination, message.Subject, message.Body));

        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            Debug.Write("Sms send");
            return Task.Run(() => Notify.With<Utils.EmailService.EmailService>(message.Destination, message.Subject, message.Body));
        }
    }

    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<ApplicationUser, int>
    {
        public ApplicationUserManager(UserStore<ApplicationUser, CustomRole, int, CustomUserLogin, CustomUserRole, CustomUserClaim> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context) 
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser, CustomRole, int, CustomUserLogin, CustomUserRole, CustomUserClaim>(context.Get<ApplicationDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser, int>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<ApplicationUser, int>
            {
                MessageFormat = "Your security code is {0}"
            });
            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<ApplicationUser, int>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = 
                    new DataProtectorTokenProvider<ApplicationUser, int>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, int>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }
}