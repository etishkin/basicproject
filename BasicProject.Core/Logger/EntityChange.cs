using System;

namespace BasicProject.Core.Logger
{
    public class EntityChange
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public string EntityName { get; set; }
        public string PropertyName { get; set; }
        public string OriginalValue { get; set; }
        public string ModifyValue { get; set; }
        public DateTime Created { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string OperationType { get; set; }

        public override string ToString()
        {
            return string.Format("[{2}][User: {8}({0})][{1}][Entity {3} with Id : {4}][{7} : \"{5}\" => \"{6}\"]", UserId,
                OperationType, Created.ToString("G"), EntityType, EntityId, OriginalValue, ModifyValue, PropertyName, UserName);
        }
    }
}