﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using BasicProject.Core.DAL;
using log4net;
using Microsoft.AspNet.Identity;

namespace BasicProject.Core.Logger
{
    internal interface ILogger
    {
        void Run();
    }

    public class Logger : ILogger
    {
        Dictionary<EntityState, string> _operationTypes;

        private readonly IDbContext _dbContext;
        private readonly ILog Log = LogManager.GetLogger("SystemAppender");

        public Logger(IDbContext dbContext)
        {
            _dbContext = dbContext;
            InitOperationTypes();
        }

        private class UserData
        {
            public string Id { get; set; }
            public string Name { get; set; }

            public UserData(string id, string name)
            {
                Id = id;
                Name = name;
            }
        }

        public void Run()
        {
            LogChangedEntities(EntityState.Added);
            LogChangedEntities(EntityState.Modified);
            LogChangedEntities(EntityState.Deleted);
        }

        private void InitOperationTypes()
        {
            _operationTypes = new Dictionary<EntityState, string>
                {
                    {EntityState.Added, "Добавление"},
                    {EntityState.Deleted, "Удаление"},
                    {EntityState.Modified, "Изменение"}
                };
        }

        private string GetOperationName(EntityState entityState)
        {
            return _operationTypes[entityState];
        }

        private void LogChangedEntities(EntityState entityState)
        {
            IEnumerable<DbEntityEntry> dbEntityEntries = _dbContext.GetChangeTracker().Entries().Where(x => x.State == entityState);
            foreach (var dbEntityEntry in dbEntityEntries)
            {
                LogChangedEntitie(dbEntityEntry, entityState);
            }
        }

        private UserData GetCurrentUser()
        {
            if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity != null)
            {
                return new UserData(HttpContext.Current.User.Identity.GetUserId(), HttpContext.Current.User.Identity.GetUserName());
            }
            return new UserData("0", "<not_found>");
        }

        private void LogChangedEntitie(DbEntityEntry dbEntityEntry, EntityState entityState)
        {
            int enitityId = ApplicationDbContext.GetKeyValue(dbEntityEntry.Entity);

            Type type = dbEntityEntry.Entity.GetType();

            IEnumerable<string> propertyNames = entityState == EntityState.Deleted
                                                    ? dbEntityEntry.OriginalValues.PropertyNames
                                                    : dbEntityEntry.CurrentValues.PropertyNames;

            var currentUser = GetCurrentUser();

            foreach (var propertyName in propertyNames)
            {
                DbPropertyEntry property = dbEntityEntry.Property(propertyName);

                if (entityState == EntityState.Modified && !property.IsModified)
                    continue;

                var change = new EntityChange
                {
                    UserId = currentUser.Id,
                    UserName = currentUser.Name,
                    Created = DateTime.Now,
                    EntityName = string.Empty,
                    EntityType = type.ToString(),
                    EntityId = enitityId.ToString(),
                    PropertyName = propertyName,
                    OriginalValue =
                        entityState != EntityState.Added && property.OriginalValue != null
                            ? property.OriginalValue.ToString()
                            : string.Empty,
                    ModifyValue =
                        entityState != EntityState.Deleted && property.CurrentValue != null
                        ? property.CurrentValue.ToString()
                        : string.Empty,
                    OperationType = GetOperationName(entityState),
                };
                Log.Info(change.ToString());
            }
        }
    }
}